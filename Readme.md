This Visual Studio solution is a skelaton project that includes all the following features:

- Dependancy Injection using an Autofac container with automatic dependancy registration.
- Elmah error logging
- Service and Repository layers with the domain models and repositories in a seperate project