﻿
Imports System.ComponentModel.DataAnnotations

Namespace ViewModels
    Public Class CustomerIndex
        Public Property CustomerID As Integer
        Public Property CustomerName As String
        Public Property CompanyName As String

        Public Property Address1 As String
        Public Property PhoneNumber As String

    End Class


    Public Class CustomerCreate

        <HiddenInput>
        Public Property CustomerID As Integer

        <Required>
        <StringLength(50)>
        Public Property FirstName As String

        <StringLength(60)>
        <Required>
        Public Property LastName As String

        <StringLength(100)>
        Public Property CompanyName As String

        <StringLength(100)>
        Public Property Address1 As String

        <StringLength(100)>
        Public Property Address2 As String

        <StringLength(60)>
        Public Property City As String

        <StringLength(2)>
        Public Property State As String

        <StringLength(10)>
        <DataType(DataType.PostalCode)>
        Public Property Zip As String

        <StringLength(250)>
        <DataType(DataType.EmailAddress)>
        Public Property EmailAddress As String

        <StringLength(30)>
        <DataType(DataType.PhoneNumber)>
        Public Property PhoneNumber As String

        <DataType(DataType.DateTime)>
        Public Property AddDate As DateTime

        <StringLength(40)>
        Public Property AddUser As String

        <Required>
        <StringLength(1, MinimumLength:=1)>
        Public Property Status As String



    End Class

End Namespace

