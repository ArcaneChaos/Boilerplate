﻿Imports System.Web.Mvc
Imports dcdrAdmin.Services
Imports dcdrAdmin.Services.Customer
Imports dcdrAdmin.ViewModels
Imports dcdrLib.DAL.Repositories
Imports dcdrLib.Models

Namespace Controllers
    Public Class CustomerController
        Inherits Controller

#Region "DI Paramaters"
        Private _customerService As ICustomerService(Of Customer) = Nothing
#End Region


        Public Sub New(CustomerService As ICustomerService(Of Customer))
            Me._customerService = CustomerService

        End Sub


        ' GET: Customer
        Function Index() As ActionResult
            Dim CustomerVM As List(Of CustomerIndex)

            Dim CustList = _customerService.FindAll()

            CustomerVM = Mapper.Map(Of List(Of Customer), List(Of CustomerIndex))(CustList)

            Return View(CustomerVM)
        End Function

        ' GET: Customer/Details/5
        Function Details(ByVal id As Integer) As ActionResult


            Return View()
        End Function

        ' GET: Customer/Create
        Function Create() As ActionResult
            Return View(New CustomerCreate)
        End Function

        ' POST: Customer/Create
        <HttpPost()>
        <ValidateAntiForgeryToken>
        Function Create(ByVal postdata As CustomerCreate) As ActionResult
            Try
                If ModelState.IsValid Then
                    Dim NewCustomer = Mapper.Map(Of CustomerCreate, Customer)(postdata)
                    _customerService.Create(NewCustomer)
                End If


            Catch
                Return View(postdata)
            End Try
            Return View(postdata)
        End Function

        ' GET: Customer/Edit/5
        Function Edit(ByVal id As Integer) As ActionResult
            Return View()
        End Function

        ' POST: Customer/Edit/5
        <HttpPost()>
        Function Edit(ByVal id As Integer, ByVal collection As FormCollection) As ActionResult
            Try
                ' TODO: Add update logic here

                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function

        ' GET: Customer/Delete/5
        Function Delete(ByVal id As Integer) As ActionResult
            Return View()
        End Function

        ' POST: Customer/Delete/5
        <HttpPost()>
        Function Delete(ByVal id As Integer, ByVal collection As FormCollection) As ActionResult
            Try
                ' TODO: Add delete logic here

                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function
    End Class

    Public Interface ILoggerFactory
    End Interface
End Namespace