﻿Imports dcdrLib.DAL


Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Dim db = New MainDBContext()

    Function Index() As ActionResult
        Return View()
    End Function

    Function About() As ActionResult
        ViewData("Message") = "Your application description page."

        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "Your contact page."

        Return View()
    End Function
End Class
