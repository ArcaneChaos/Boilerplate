﻿Imports AutoMapper
Imports dcdrAdmin.ViewModels
Imports dcdrLib.Models

Public Module AutoMapperConfig
    Public MapperConfiguration As MapperConfiguration
    Public Mapper As IMapper

    Public Sub RegisterMappings()

        MapperConfiguration = New MapperConfiguration(
            Function(c)
                c.CreateMap(Of Customer, CustomerIndex)() _
                    .ForMember(Function(d) d.CustomerName, Sub(opt) opt.MapFrom(Function(s) s.FirstName & " " & s.LastName))



                Return c
            End Function
         )


        'MapperConfiguration.AssertConfigurationIsValid()
        Mapper = MapperConfiguration.CreateMapper()





    End Sub

End Module