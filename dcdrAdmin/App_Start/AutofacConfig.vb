﻿Imports Autofac
Imports System.Web.Mvc
Imports Autofac.Integration.Mvc
Imports System.Reflection
Imports dcdrAdmin.Services
Imports dcdrLib.DAL.Repositories


Public Module AutofacConfig



    Public Sub CreateContainer()
        Dim builder As New ContainerBuilder()
        Dim assembly As Assembly = Assembly.GetExecutingAssembly()

        RegisterServices(builder, assembly)
        RegisterMvc(builder, assembly)

        ' Enable property injection in view pages.
        builder.RegisterSource(New ViewRegistrationSource())


        Dim container As IContainer = builder.Build()

        DependencyResolver.SetResolver(New AutofacDependencyResolver(container))


    End Sub

    Private Sub RegisterServices(ByRef builder As ContainerBuilder, assemblyToScan As Assembly)

        ' scan current assembly looking for services and repositories to register
        builder.RegisterAssemblyTypes(assemblyToScan).
                    Where(Function(t) t.Name.EndsWith("Service")).AsImplementedInterfaces

        builder.RegisterAssemblyTypes(GetType(CustomerRepository).Assembly).
                Where(Function(t) t.Name.EndsWith("Repository")).AsImplementedInterfaces()
    End Sub

    Private Sub RegisterMvc(ByRef builder As ContainerBuilder, assembly As Assembly)
        ' Register Common MVC Types
        builder.RegisterModule(Of AutofacWebTypesModule)()

        ' Register MVC FIlters
        builder.RegisterFilterProvider()

        'Register MVC COntrollers
        builder.RegisterControllers(assembly)
    End Sub








End Module
