﻿Imports dcdrLib
Imports dcdrLib.DAL.Repositories

Namespace Services.Customer
    Public Class CustomerService
        Implements ICustomerService(Of Models.Customer)

        Private ReadOnly _iCustomerRepository As IRepository(Of Models.Customer)

        Public Sub New(CustomerRepository As IRepository(Of Models.Customer))
            _iCustomerRepository = CustomerRepository
        End Sub

        Public Function Create(customer As Models.Customer) As Integer Implements ICustomerService(Of Models.Customer).Create
            Throw New NotImplementedException()
        End Function

        Public Function FindAll() As IEnumerable(Of Models.Customer) Implements ICustomerService(Of Models.Customer).FindAll
            Return _iCustomerRepository.FindAll()
        End Function

        Public Function FindById(id As Integer) As Models.Customer Implements ICustomerService(Of Models.Customer).FindById
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace

