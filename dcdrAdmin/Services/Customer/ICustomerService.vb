﻿

Namespace Services.Customer

    Public Interface ICustomerService(Of t)
        Function Create(T As t) As Integer
        Function FindAll() As IEnumerable(Of t)
        Function FindById(id As Integer) As t
    End Interface

End Namespace

