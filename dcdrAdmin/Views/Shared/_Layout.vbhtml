﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Judicial Data Customer | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="Developed By M Abdur Rokib Promy">
    <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
    @Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/modernizr")
    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")
    @Scripts.Render("~/bundles/global")

</head>
<body class="skin-black fixed">
    <!-- header logo: style can be found in header.less -->
    <header class="header">
        @Html.ActionLink("Data Customer Admin", "Index", "Home", Nothing, New With {.class = "logo"})

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            @Html.Partial("_TopRightNav")

        </nav>
    </header>

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel text-center">
                        <img src="~/Content/Images/ctjudlogo.jpg" alt="Judicial Logo" />
                </div>
                <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..." />
                        <span class="input-group-btn">
                            <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="active">
                        <a href="@Url.Action("Index", "Customer")">
                            <i class="fa fa-users"></i> <span>Customers</span>
                        </a>
                    </li>
                    <li>
                        <a href="general.html">
                            <i class="fa fa-usd"></i> <span>Rate Information</span>
                        </a>
                    </li>

                    <li>
                        <a href="basic_form.html">
                            <i class="fa fa-file-text"></i> <span>Reports</span>
                        </a>
                    </li>


                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <aside class="right-side">

            <!-- Main content -->
            <section class="content">
                @RenderBody()
            </section>
            <div class="footer-main">
                Copyright <i class="fa fa-copyright"></i> CT Judicial Branch,  @Date.Now().Year
            </div>
        </aside><!-- /.right-side -->

    </div><!-- ./wrapper -->

</body>
</html>