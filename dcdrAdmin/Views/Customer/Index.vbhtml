﻿@ModelType IEnumerable(Of dcdrAdmin.ViewModels.CustomerIndex)
@Code
ViewData("Title") = "Index"
End Code

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.CustomerName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CompanyName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Address1)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.PhoneNumber)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CustomerName)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CompanyName)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Address1)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.PhoneNumber)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = item.CustomerID }) |
            @Html.ActionLink("Details", "Details", New With {.id = item.CustomerID }) |
            @Html.ActionLink("Delete", "Delete", New With {.id = item.CustomerID })
        </td>
    </tr>
Next

</table>
