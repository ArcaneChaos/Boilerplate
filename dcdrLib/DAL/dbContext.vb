﻿Imports dcdrLib.Models
Imports System.Data.Entity
Imports System.Data.Entity.ModelConfiguration.Conventions


Namespace DAL
    Public Class MainDBContext
        Inherits DbContext

        Public Property Customers As DbSet(Of Customer)


        Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)

            modelBuilder.Conventions.Remove(Of PluralizingTableNameConvention)()
        End Sub

    End Class

End Namespace
