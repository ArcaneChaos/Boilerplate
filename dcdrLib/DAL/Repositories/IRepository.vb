﻿
Namespace DAL.Repositories
    Public Interface IRepository(Of t)
        Function Create(t As t) As Integer
        Function FindAll() As IEnumerable(Of t)
        Function FindByID(id As Integer) As t

    End Interface
End Namespace
