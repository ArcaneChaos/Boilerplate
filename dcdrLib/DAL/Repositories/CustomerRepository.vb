﻿Imports dcdrLib.Models

Namespace DAL.Repositories
    Public Class CustomerRepository
        Implements IRepository(Of Customer)

        Dim db As MainDBContext = New MainDBContext()

        Public Function Create(customer As Customer) As Integer Implements IRepository(Of Customer).Create
            If customer IsNot Nothing Then
                db.Customers.Add(customer)
                db.SaveChanges()
                Return True
            End If

            Return False
        End Function

        Public Function FindAll() As IEnumerable(Of Customer) Implements IRepository(Of Customer).FindAll
            Return db.Customers.ToList()

        End Function

        Public Function FindById(id As Integer) As Customer Implements IRepository(Of Customer).FindByID
            Return db.Customers.Find(id)
        End Function

    End Class
End Namespace
